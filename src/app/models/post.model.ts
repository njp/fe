import * as moment from 'moment';
import { User } from './user.model';

export interface Post {
  id?: number;
  user: {
    fullName: string,
    handle: string
  },
  date: moment.Moment,
  title: string,
  image: string,
  description: string,
  likedBy: User[];
}
