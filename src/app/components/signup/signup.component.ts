import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {

  constructor(
    private http: HttpClient
  ) { }

  model = {
    email: '',
    username: '',
    password: ''
  };

  onSubmit() {
    this.http.post('http://localhost:8080/users/sign-up', this.model, { observe: 'response' })
      .subscribe(resp => {
        if (resp.status === 201) {
          alert('Check your email');
          window.location.href = 'https://gmail.com';
        }
      });
  }
}
