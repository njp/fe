import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-user-debug',
  templateUrl: './user-debug.component.html'
})
export class UserDebugComponent implements OnInit {

  constructor(
    private userService: UserService
  ) { }

  get users() { return this.userService.users; }

  ngOnInit() {
    this.userService.fetchUsers();
  }

  onFollow(userId: string) {
    // if (userId === this.userService.user.id) {
      this.userService.followUser(userId);
    // } else {
      // alert('Ca/nt follow yourself');
    // }
  }

  onUnfollow(userId: string) {
    this.userService.unfollowUser(userId);
  }
}
