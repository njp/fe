import { Component } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { UserService } from 'app/services/user.service';
import { Router } from '@angular/router';

interface LoginFormModel {
  username: string;
  password: string;
  rememberMe: boolean;
}

const defaults: LoginFormModel = {
  username: '',
  password: '',
  rememberMe: false
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private router: Router
  ) { }

  model: LoginFormModel = Object.assign({}, defaults);

  onSubmit() {
    this.http.post('http://localhost:8080/login', {...this.model, rememberMe: undefined}, {observe: 'response'})
      .subscribe(resp => {
        console.log('bitch what');
        if (resp.status === 200) {
          console.log(resp.headers);
          const token  = resp.headers.get('Authorization');
          console.log(token);
          this.userService.token = token || "";
          this.userService.fetchUsers();
          this.router.navigate(['/home']);
        } else {
          alert('Invalid login data');
        }
      },
      err => {
        console.log(err);
        alert('Invalid login');
      }
      );
    // console.log(this.model);
  }
}
