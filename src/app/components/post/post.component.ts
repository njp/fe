import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Post } from '@models/post.model';
import { HttpClient } from '@angular/common/http';
import { PostService } from 'app/services/post.service';
import { UserService } from 'app/services/user.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  constructor(
    private postService: PostService,
    private userService: UserService
  ) { }

  @Input() post: Post;

  comment = "";

  get isLiked() {
    return this.post.likedBy.some(user => user.username === this.userService.user.username);
    return true;
  }

  ngOnInit() {

  }

  likePost() {
    this.postService.likePost(this.post.id.toString());
  }

  unlikePost() {
    this.postService.unlikePost(this.post.id.toString());
  }

  addComment() {
    console.log(this.comment);
    if (!(this.comment === '')) {
      this.postService.comment(this.post.id, this.comment);
    }
  }
}
