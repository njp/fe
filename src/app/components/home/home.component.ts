import { Component, OnInit } from "@angular/core";
import { HttpClient, HttpRequest, HttpResponse } from '@angular/common/http';
import { Post } from '@models/post.model';
import { PostService } from 'app/services/post.service';
import { UserService } from 'app/services/user.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit{

  model: {
    image: File,
    description: string
  } = {
    image: undefined,
    description: ''
  };

  constructor(
    private postService: PostService,
    private userService: UserService
  ) { }

  get posts() {
    return this.postService.posts;
  }

  get user() {
    return this.userService.user;
  }

  ngOnInit() {
    this.postService.fetchPosts();
  }

  onSubmit() {
    console.log('FORM SUBMITTED');
    this.postService.addPost(this.model);
  }

  onLogout() {
    console.log('logout trigger');
    this.userService.logout();
  }
}
