import { Component, OnInit } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { VirtualTimeScheduler } from 'rxjs';
import { User } from '@models/user.model';

@Component({
  selector: 'app-messaging',
  templateUrl: './messaging.component.html'
})
export class MessagingComponent implements OnInit {

  constructor(
    private userService: UserService
  ) { }

  private ws: WebSocket;

  selectedUser: User;

  message: string;

  get isWsOpen() {
    return this.ws.readyState === 1;
  }

  get users() {
    return this.userService.users.filter(u => u.username !== this.userService.user.username);
  }

  setSelected(user: User) {
    console.log(user);
    this.selectedUser = user;
  }

  messages: Array<{from: '', to: '', message: ''}> = [];

  sendMessage() {
    this.ws.send(JSON.stringify({
      type: 'SEND',
      from: this.userService.user.username,
      to: this.selectedUser.username,
      message: this.message
    }));
    this.message = '';
  }

  handleMessage = e => {
    const newMessages = JSON.parse(e.data);
    console.log(newMessages);
    this.messages = [
      ...this.messages,
      ...newMessages
    ];
  }

  ngOnInit() {
    this.userService.fetchUsers();
    this.ws = new WebSocket('ws://localhost:8080/ws');
    this.ws.onmessage = this.handleMessage;
    this.ws.onerror = e => console.error(e);
    this.ws.onopen = () =>
      this.ws.send(JSON.stringify({
        type: 'AUTH',
        token: this.userService.token
    }));
  }
}
