import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { UserService } from './services/user.service';

@Injectable()
export class JwtAuthInterceptor implements HttpInterceptor {

  constructor(
    private userService: UserService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    if (this.userService.isLoggedIn) {
      console.log(this.userService.token);
      request = request.clone({
        setHeaders: {
          'Authorization': this.userService.token
        }
      });
    }
    return next.handle(request);
  }

}
