import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '@components/home/home.component';
import { LoginComponent } from '@components/login/login.component';
import { AuthGuard } from './auth.guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtAuthInterceptor } from './auth.interceptor';
import { SignupComponent } from '@components/signup/signup.component';
import { MessagingComponent } from '@components/messaging/messaging.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [ AuthGuard ] },
  { path: 'messages', component: MessagingComponent, canActivate: [ AuthGuard ] },
  { path: 'login', component: LoginComponent },
  { path: 'signup',  component: SignupComponent },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
 imports: [ RouterModule.forRoot(appRoutes, { enableTracing: false }) ],
 providers: [
   AuthGuard,
   {
     provide: HTTP_INTERCEPTORS,
     useClass: JwtAuthInterceptor,
     multi: true
   }
  ],
 exports: [ RouterModule ]
})
export class AppRoutingModule { }
