import { Component, OnInit } from '@angular/core';
import { UserService } from './services/user.service';
import { Router } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'fe';

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

    ngOnInit() {
      this.userService.loadToken();
      console.log('User is logged in: ', this.userService.isLoggedIn);
      if (this.userService.isLoggedIn) {
        this.router.navigate['/home'];
      } else {
        this.router.navigate['/login'];
      }
    }
}
