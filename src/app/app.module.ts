import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PostComponent } from '@components/post/post.component';
import { LoginComponent } from '@components/login/login.component';
import { HomeComponent } from '@components/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { UserService } from './services/user.service';
import { HttpClientModule } from '@angular/common/http';
import { PostService } from './services/post.service';
import { UserDebugComponent } from '@components/user-debug/user-debug.component';
import { SignupComponent } from '@components/signup/signup.component';
import { MessagingComponent } from '@components/messaging/messaging.component';

@NgModule({
  declarations: [
    AppComponent,
    PostComponent,
    LoginComponent,
    HomeComponent,
    UserDebugComponent,
    SignupComponent,
    MessagingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [
    UserService,
    PostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
