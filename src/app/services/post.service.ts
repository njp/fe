import { Injectable } from '@angular/core';
import { Post } from '@models/post.model';
import { HttpClient, HttpRequest, HttpResponse } from '@angular/common/http';

@Injectable()
export class PostService {

  private _posts: Post[] = [];

  constructor(
    private http: HttpClient
  ) { }

  get posts() {
    return this._posts;
  }

  fetchPosts() {
    this.http.get('http://localhost:8080/posts')
    .subscribe((posts: Post[]) => this._posts = posts);
  }

  addPost(post: {image: File, description: string}) {
    const formData = new FormData();
    formData.append('image', post.image);
    const request = new HttpRequest('POST', 'http://localhost:8080/upload', formData);
    this.http.request(request)
      .subscribe((response: HttpResponse<{imageId: string}>) => {
        console.log(response);
        if (response.status === 201) {
          this.http.post('http://localhost:8080/posts', {
            imageId: response.body.imageId,
            description: post.description
          }).subscribe(resp => {
            console.log(resp);
            this.fetchPosts();
          });
        }
      });
  }

  likePost(postId: string) {
    this.http.post(`http://localhost:8080/posts/${postId.toString()}/like`, null)
    .subscribe(() => this.fetchPosts());
  }

  unlikePost(postId: string) {
    this.http.get(`http://localhost:8080/posts/${postId.toString()}/unlike`)
    .subscribe(() => this.fetchPosts());
  }

  comment(postId: number, comment: string) {
    this.http.post(`http://localhost:8080/posts/${postId.toString()}/comment`, {comment})
    .subscribe(() => this.fetchPosts());
  }
}
