import { HttpClient } from '@angular/common/http';
import { User } from '@models/user.model';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class UserService {

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  _token: string = "";
  get token() { return this._token; }
  set token(token: string) {
    localStorage.setItem('x-auth-token', token);
    this._token = token;
  }

  loadToken() {
    const token = localStorage.getItem('x-auth-token');
    this._token = token !== null ? token : "";
    return this.isLoggedIn;
  }

  get isLoggedIn() {
    return this.token !== "";
  }

  private _users = [];
  user: any;

  get users() { return this._users; }

  fetchUsers() {
    this.http.get(`http://localhost:8080/users`)
      .subscribe((users: User[]) => this._users = users);
    this.http.get('http://localhost:8080/users/me')
      .subscribe(u => this.user = u);
  }

  followUser(userId: string) {
    this.http.get(`http://localhost:8080/users/${userId}/follow`)
      .subscribe(() => this.fetchUsers());
  }

  unfollowUser(userId: string) {
    this.http.get(`http://localhost:8080/users/${userId}/unfollow`)
      .subscribe(() => this.fetchUsers());
  }

  logout() {
    this.token = "";
    console.log('triggering router');
    this.router.navigate(['/login']);
  }
}
